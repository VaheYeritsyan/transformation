package com.vahe.demo.controller;

import lombok.Value;

import java.util.List;

@Value
public class TransformationResponse {
    String name;
    List<String> items;
    long timestamp;
}
