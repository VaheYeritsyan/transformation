package com.vahe.demo.controller;

import lombok.Value;

import java.util.List;

@Value
public class TransformationRequest {
    String name;
    List<String> items;
}
