package com.vahe.demo.controller;

import com.vahe.demo.service.TransformationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
@RequiredArgsConstructor
public class TransformationController {
    private final TransformationService transformationService;

    @PostMapping("transformation")
    @ResponseStatus(HttpStatus.OK)
    public TransformationResponse transform(@RequestBody TransformationRequest transformationRequest) {
        return transformationService.doTransform(transformationRequest);
    }

}