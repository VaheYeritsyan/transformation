package com.vahe.demo.service;

import com.vahe.demo.controller.TransformationRequest;
import com.vahe.demo.controller.TransformationResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransformationService {

    public TransformationResponse doTransform(TransformationRequest transformationRequest) {
        List<String> list = transformationRequest.getItems();
        List<String> list2 = new ArrayList<>();
        for (int j = 0; j < list.size(); j++) {
            if (list.get(j).matches("[a-zA-Z_]+")) {
                if (list.get(j).contains("_")) {
                    StringBuilder builder = new StringBuilder(list.get(j));
                    builder.replace(0, 0, String.valueOf(Character.toUpperCase(builder.charAt(0))));
                    for (int i = 0; i < builder.length(); i++) {
                        if (builder.charAt(i) == '_') {
                            builder.deleteCharAt(i);
                            builder.replace(
                                    i, i + 1,
                                    String.valueOf(
                                            Character.toUpperCase(
                                                    builder.charAt(i))));
                        }
                    }
                    list.set(j, String.valueOf(builder));
                }
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad Request");
            }
            if (!list2.contains(list.get(j))) {
                list2.add(list.get(j));
            }
        }
        return new TransformationResponse(transformationRequest.getName(), list2, System.currentTimeMillis());
    }
}
